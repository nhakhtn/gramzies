<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ajax extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->helper('url');
        $this->load->model('users');
        $this->load->library('parser');
        $this->load->library('session');
        // $this->load->library('auth');
        // $this->load->library('excellib');
    }

    private function gettime() {
        $return['error'] = false;
        $return['sdate'] = microtime_float(date("Y-m-d H:i:s"));
        $return['ldate'] = date("Y-m-d H:i:s");

        echo json_encode($return);
    }

    private function companies() {

        $sid = $this->input->post('sid');
        $return['companies'] = $this->main->getCompaniesByState($sid);
        $return['error'] = false;
        echo json_encode($return);
    }

    private function getinfo() {
        $cid = $this->input->post('cid');
        $return['rebates'] = $this->main->GetRebates($cid);
        $return['incentives'] = $this->main->GetIncentives($cid);

        $return['error'] = false;
        echo json_encode($return);
    }

    private function login() {
        
        $fields = $this->input->post('fields');
        
        
        $user = $fields['username'];
        $pass = $fields['password'];
        
        $loggedin = $this->auth->authenticate($user, $pass);

        $return['error'] = true;
        if ($loggedin) {
            $return['error'] = false;
        } else {
            $return['message'] = 'Wrong username or password';
        }
        echo json_encode($return);
    }

    private function double() {
        $cid = $this->input->post('id');

        $rebates = $this->main->GetRebates($cid);
        $incentives = $this->main->GetIncentives($cid);
        $company = $this->main->getCompaniesByID($cid);

        if (count($company) <= 0) {
            $return['error'] = true;
            echo json_encode($return);
            return false;
        }

        unset($company[0]['id']);
        $this->db->insert('companies', $company[0]);

        $ncid = $this->db->insert_id();



        for ($i = 0; $i < count($rebates); $i++) {

            unset($rebates[$i]['id']);
            $rebates[$i]['cid'] = $ncid;
            $this->db->insert('rebates', $rebates[$i]);
        }


        for ($i = 0; $i < count($incentives); $i++) {

            unset($incentives[$i]['id']);
            $incentives[$i]['cid'] = $ncid;
            $this->db->insert('incentives', $incentives[$i]);
        }

        $return['id'] = $ncid;

        $return['error'] = false;
        echo json_encode($return);
    }

    private function move() {
        $id = $this->input->post('id');
        $sid = $this->input->post('sid');


        $data = array('sid' => $sid);

        $this->db->where('id', $id);
        $this->db->update('companies', escape_arr($data));

        $return['error'] = false;
        echo json_encode($return);
    }

    private function updateinfo() {
        $id = $this->input->post('id');
        $table = $this->input->post('table');
        $cid = 0;
        $fields = $this->input->post('fields');
        $id = (int) $id;

        if ($table != 'users' && $table != 'companies') {
            $cid = $this->input->post('cid');
        }

        $rid = $id;

        if ($id == 0 || $id == '0') {  // Add new
            $data = $fields;
            if ($table != 'users' && $table != 'companies' && $table != 'states') {
                $data['cid'] = $cid;
            } else if ($table == 'users') {
                if ($data['password'] == 'Hidden') {
                    unset($data['password']);
                } else {
                    $data['password'] = hash_hmac("sha512", "salt" . $data['password'], 'qkmH5gaYPs35PD6xfG8Kq3Dd46N1mz32');
                }
            }

            $this->db->insert($table, escape_arr($data));
            $rid = $this->db->insert_id();
        } else { // Edit
            $data = $fields;

            if ($table == 'users') {
                if ($data['password'] == 'Hidden') {
                    unset($data['password']);
                } else {
                    $data['password'] = hash_hmac("sha512", "salt" . $data['password'], 'qkmH5gaYPs35PD6xfG8Kq3Dd46N1mz32');
                }
            }

            $this->db->where('id', $id);
            $this->db->update($table, escape_arr($data));
        }

        $return['id'] = $rid;
        $return['error'] = false;
        echo json_encode($return);
    }

    private function deleteinfo() {
        $id = $this->input->post('id');
        $table = $this->input->post('table');
        $id = (int) $id;


        $this->db->delete($table, array('id' => $id));


        $return['error'] = false;
        echo json_encode($return);
    }

    private function signup() {



        $fields = $this->input->post('fields');
       
        $this->users->insert_user_details($fields);
        //$this->users->signup($fields);


        $return['error'] = false;
        echo json_encode($return);
    }
 
    private function contact() {



        $fields = $this->input->post('fields');

        SendMail(getoption('adminmail'), $fields['subject'], $fields['message'], $fields['email'], $fields['name']);


        $return['error'] = false;
        echo json_encode($return);
    }

    private function updatecredit() {

        $igid = $this->input->post('igid');
        $free = $this->input->post('free');
        $off = $this->input->post('off');

        $this->users->UpdateCredit($free, $off, $igid);


        $return['error'] = false;
        echo json_encode($return);
    }

    private function deleteuser() {

        $igid = $this->input->post('igid');

        $this->users->DeleteUser($igid);


        $return['error'] = false;
        echo json_encode($return);
    }

    private function updateoption() {
        $fields = $this->input->post('fields');

        foreach ($fields as $key => $value) {
            setoption($key, $value);
        }

        $return['error'] = false;
        echo json_encode($return);
    }

    public function index() {
        $ajax = $this->input->post('ajax');


        if ($ajax) {
            $callname = '';

            if (is_callable(array($this, $ajax))) {
                call_user_func_array(array($this, $ajax), array());
                return true;
            } else {
                $return['error'] = true;
                $return['message'] = 'function not found';
                echo json_encode($return);
                return false;
            }
        } else {
            $return['error'] = true;
            $return['message'] = 'function is nothing';

            echo json_encode($return);
            return false;
        }
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
