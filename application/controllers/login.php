<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    public function index() {


        $options = $this->options;

        $pagedata = array();

        $pagedata = array_merge($options, $pagedata);
        $content="";
        $keyword='';

        $data = GetHeader('Login',$content,$keyword, $pagedata, true);

        $this->parser->parse('front/fheader', $data);
        $this->parser->parse('admin/flogin', $data);
        $this->parser->parse('front/ffooter', $data);
    }

}

