<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cron extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->helper('igr');
        $this->load->model('users');
        $this->load->library('parser');
    }

    public function index() {
        //SendMail('rezanew@gmail.com', 'testmail', 'This is my test message');
        //$this->users->UpdateAfterOrder(10, '13460080');
    }

    public function daily() {



        $sdate = date("Y-m-d");

        //  $sdate = '2013-11-27';

        $users = array();
        $all = $this->input->get('all', '');
        if($all == 'yes') {
            $users = $this->users->GetUsers();
        } else {
            $igid = $this->input->get('igid', 0);
            if($igid != 0) {
                $users = $this->users->GetUserByIgid($igid);
            } else {
                if (getoption('cron_test')) {
                    $users = $this->users->GetUsersByRandom(2);
                } else {
                    $users = $this->users->GetUsersBySdate($sdate);
                }
            }
        }

        $images = array();


        foreach ($users as $user) {
            $images_user = getImages($user);




            $images[] = $images_user;
        }

        if (count($users) == 0) {
            $images = array('error' => true, 'message' => 'There is no user for today.');
        }

        //$user = $this->users->IsUserExist($igid);
//        if ($user) {
//             
//             
//            try {
//              $images = getImages($user);
//               
//             //  $images=  ChargeCard($user, 10);
//               
//            } catch (Exception $exc) {
//                $images = array('error' => true, 'message' => $exc->getMessage());
//            }
//        }



        if (is_array($images)) {
            $images = json_encode($images);
        }

        header('Content-type: application/json');
        echo $images;




        // echo count($result->data) . '<br>';
    }

}

