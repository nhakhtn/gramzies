<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends CI_Controller {

    public function index() {

        if (!CheckLoggedIn()) {
            redirect(base_url() . 'login');
        }



        $users = $this->users->GetUsers();
        $orders = $this->orders->GetOrders();

        $options = $this->options;

        $pagedata = array('user' => $users, 'order' => $orders);

        $pagedata = array_merge($options, $pagedata);
        $content="";
        $keyword='';

        $data = GetHeader('Admin',$content,$keyword, $pagedata, true);

        $this->parser->parse('front/fheader', $data);
        $this->parser->parse('admin/fadmin', $data);
        $this->parser->parse('front/ffooter', $data);
    }

}

