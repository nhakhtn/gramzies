<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Signup extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->library('session');
          $this->load->model('users');

        // $this->load->model('main');
    }

    public function index() {

         ShowSignup(); 

    }

    public function callback() {
        
        $code = $this->input->get('code');

        if (!$code) {
            redirect(base_url() . INDEX . 'signup');
        }
        $instagram = new Instagram(array(
                    'apiKey' => getoption('ig_key'),
                    'apiSecret' => getoption('ig_secret'),
                    'apiCallback' => base_url() . INDEX . 'signup/callback'
                ));
        

        $data = $instagram->getOAuthToken($code);

        if (!isset($data->code)) {

            $instagram->setAccessToken($data->access_token);

            $user = $instagram->getUser();
            $user = $user->data;
            $token = $data->access_token;
            $uid   = $user->id;
            $this->session->set_userdata('ig_token', $token); 
                   
           $fields = array('igtoken' => $token, 'igid'   => $uid);
           $this->users->update_user_instagram($fields);

             
           // redirect(base_url());
            redirect(base_url() . INDEX );
            //ShowSignup($user, $token);
            exit();
            
        } else {
            
            redirect(base_url() . INDEX . 'signup');
            
        }
          
    }
    public function instagram(){
        
        $callback = base_url() . INDEX . 'signup/callback';
        $callback =  strtolower($callback);
        
        $callback = str_replace('http://www.', 'http://', $callback);
        $callback = str_replace('www.', '', $callback);
     
        $instagram = new Instagram(array(
                    'apiKey' => getoption('ig_key'),
                    'apiSecret' => getoption('ig_secret'),
                    'apiCallback' => $callback
                ));


        $ig_token = $this->session->userdata('ig_token');
     
            
       if (!empty($ig_token)) {

            $instagram->setAccessToken($ig_token);

           $user = $instagram->getUser();
            $meta = $user->meta;

            if ($meta->code == 200) {
               redirect($instagram->getLoginUrl());
          //  exit();
             //  ShowSignup($user->data, $ig_token);

                return false;
            }
        }
        redirect($instagram->getLoginUrl());
    }


}
