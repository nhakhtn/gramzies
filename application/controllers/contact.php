<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Contact extends CI_Controller {

    public function index() {

        $content="Please contact our customer service department. We aim to please. Please ask us anything.";
        $keyword='';
        $data = GetHeader('Gramies Contact Us',$content,$keyword, array());
        
        $this->parser->parse('front/fheader', $data);
        $this->parser->parse('front/fcontact', $data);
        $this->parser->parse('front/ffooter', $data);
    }

}

