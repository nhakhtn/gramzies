<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class About_Gramzies extends CI_Controller {

    public function index() {

        $content = "A monthly service that prints you Instagram photos and delivers them automatically each month.";
        $keywords = "how, print, instagram, photos";

        $data = GetHeader('About Gramzies',$content, $keywords, array());
        
        $this->parser->parse('front/fheader', $data);
       // $this->parser->parse('front/fhome', $data);
         $this->parser->parse('front/fabout-us', $data);
        $this->parser->parse('front/ffooter', $data); 
      
    }

}

