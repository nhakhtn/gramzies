<link rel="stylesheet" href="{burl}bootstrap/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" type="text/css" href="{burl}js/lightbox/themes/default/jquery.lightbox.css" />
<div id="subheader">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>Admin panel</h1>
                <span>Administrate your customers</span>
            </div>
        </div>
    </div>
</div>


<div id="content" class="admin">
    <div class="container" style="position: relative">



        <div class="row">




            <div class="col-md-11 col-sm-12 col-xs-12 col-md-offset-1">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#options" data-toggle="tab">Options</a></li>
                    <li><a href="#keys" data-toggle="tab">Keys</a></li>
                    <li><a href="#customers" data-toggle="tab">Customers</a></li>
                    <li><a href="#orders" data-toggle="tab">Orders</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="options">

                        <div class="form-section form-horizontal " style="border-bottom-width: 1px">
                            <h2><span class="glyphicon glyphicon-cog"></span>Site Options</h2>
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Site Name</label>
                                    <div class="col-sm-9">
                                        <input  class="form-control  inpost nec" id="op_sitename" name="op_sitename" value="{op_sitename}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Site title</label>
                                    <div class="col-sm-9">
                                        <input  class="form-control  inpost nec" id="op_title" name="op_title" value="{op_title}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Admin email</label>
                                    <div class="col-sm-9">
                                        <input  class="form-control inpost nec email" id="op_adminmail" name="op_adminmail" value="{op_adminmail}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Admin password</label>
                                    <div class="col-sm-9">
                                        <input  class="form-control inpost nec" type="password" id="op_adminpass" name="op_adminpass" value="{op_adminpass}">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Default hashtag</label>
                                    <div class="col-sm-9">

                                        <div class="input-group">
                                            <span class="input-group-addon">#</span>
                                            <input  class="form-control inpost nec" id="op_hash" name="op_hash" value="{op_hash}">

                                        </div>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" class="inpost" rel="{op_cron_test}"  id="op_cron_test" name="op_cron_test"> Cron test mode
                                            </label>
                                            <span class="help-block">In cron test mode, 2 users are chosen from database randomly.</span>
                                        </div>
                                    </div>
                                </div>



                            </div>


                        </div>

                        <div class="form-section form-horizontal ">
                            <h2><span class="glyphicon glyphicon-picture"></span>Photo Credit Options</h2>
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Base plan prints / month</label>
                                    <div class="col-sm-9">
                                        <input  class="form-control  inpost nec numeric" id="op_offcount" name="op_offcount" value="{op_offcount}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Base plan price</label>
                                    <div class="col-sm-9">
                                        <input  class="form-control  inpost nec numeric" id="op_offprice" name="op_offprice" value="{op_offprice}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Extra photo price</label>
                                    <div class="col-sm-9">
                                        <input  class="form-control  inpost nec numeric" id="op_extraprice" name="op_extraprice" value="{op_extraprice}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Free credits for all</label>
                                    <div class="col-sm-9">
                                        <input  class="form-control  inpost nec numeric" id="op_freecount" name="op_freecount" value="{op_freecount}">
                                    </div>
                                </div>





                            </div>


                        </div>
                        <div class="form-submit">

                            <button class="btn btn-default option-btn"  data-loading-text="Submitting..."><span class="glyphicon glyphicon-ok"></span> Update</button>

                        </div>

                    </div>


                    <div class="tab-pane" id="keys">

                        <div class="form-section form-horizontal " style="border-bottom-width: 1px">
                            <h2><span class="glyphicon glyphicon-lock"></span>Instagram Keys</h2>
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Instagram Key</label>
                                    <div class="col-sm-9">
                                        <input  class="form-control  inpost nec" id="op_ig_key" name="op_ig_key" value="{op_ig_key}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Instagram Secret</label>
                                    <div class="col-sm-9">
                                        <input  class="form-control  inpost nec" id="op_ig_secret" name="op_ig_secret" value="{op_ig_secret}">
                                    </div>
                                </div>



                            </div>
                        </div>


                        <div class="form-section form-horizontal " style="border-bottom-width: 1px">
                            <h2><span class="glyphicon glyphicon-lock"></span>Stripe Keys</h2>
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Test Secret Key</label>
                                    <div class="col-sm-9">
                                        <input  class="form-control  inpost nec" id="op_stipe_s" name="op_stipe_s" value="{op_stipe_s}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Test Publishable Key</label>
                                    <div class="col-sm-9">
                                        <input  class="form-control  inpost nec" id="op_stipe_p" name="op_stipe_p" value="{op_stipe_p}">
                                    </div>
                                </div>

                                <div class="form-group" style="margin-top: 30px">
                                    <label class="col-sm-3 control-label">Live Secret Key</label>
                                    <div class="col-sm-9">
                                        <input  class="form-control  inpost nec" id="op_stipe_s_live" name="op_stipe_s_live" value="{op_stipe_s_live}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Live Publishable Key</label>
                                    <div class="col-sm-9">
                                        <input  class="form-control  inpost nec" id="op_stipe_p_live" name="op_stipe_p_live" value="{op_stipe_p_live}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" class="inpost" rel="{op_stipe_test}"  id="op_stipe_test" name="op_stipe_test"> Test mode
                                            </label>
                                        </div>
                                    </div>
                                </div>



                            </div>
                        </div>



                        <div class="form-section form-horizontal ">
                            <h2><span class="glyphicon glyphicon-lock"></span>Pwinty Keys</h2>
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Merchant ID</label>
                                    <div class="col-sm-9">
                                        <input  class="form-control  inpost nec" id="op_pw_merchant" name="op_pw_merchant" value="{op_pw_merchant}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">API Key</label>
                                    <div class="col-sm-9">
                                        <input  class="form-control  inpost nec" id="op_pw_key" name="op_pw_key" value="{op_pw_key}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" class="inpost" rel="{op_pw_test}"  id="op_pw_test" name="op_pw_test"> Test mode
                                            </label>
                                        </div>
                                    </div>
                                </div>


                            </div>


                        </div>
                        <div class="form-submit">

                            <button class="btn btn-default option-btn"  data-loading-text="Submitting..."><span class="glyphicon glyphicon-ok"></span> Update</button>

                        </div>

                    </div>



                    <div class="tab-pane" id="customers"> 

                        <div class="form-section form-horizontal">
                            <h2>
                                <span class="glyphicon glyphicon-user"></span>
                                Customer Management
                                <a href="{burl}cron/daily/?all=yes" target="_blank">
                                <button class="btn-sendforprint btn">
                                    <span class="glyphicon glyphicon-camera"></span>
                                </button>
                                </a>
                            
                            
                            </h2>
                            




                            <div class="form-body">

                                <table class="table table-striped tb-customer" style="color: #676767" >
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>shipped</th>
                                            <th>Last Ship</th>
                                            <th>Last Run</th>
                                            <th>Status</th>
                                            <th>Log</th>
                                            <th style="color: #000">Free Credit</th>
                                            <th style="color: #000">Off Credit</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {user}

                                        <tr>
                                            <td>{sfname} {slname}</td>
                                            <td>{email}</td>
                                            <td>{shipped}</td>
                                            <td>{lastship}</td>
                                            <td>{lastrun}</td>
                                            <td>{status}</td>
                                            <td>{reason}</td>
                                            <td style="color: #000" ><div class="efree" >{freecredit}</div></td>
                                            <td style="color: #000" ><div class="eoff">{offcredit}</div></td>
                                            <td>
                                                <div class="btn-bar">
                                                <button class="btn-del btn" rel="{igid}">
                                                    <span class="glyphicon glyphicon-remove"></span>
                                                </button>
                                                <button  rel="{igid}" class="btn-edit btn">
                                                    <span class="glyphicon glyphicon-pencil"></span>
                                                    <span class="glyphicon glyphicon-ok"></span>
                                                </button>
                                                <a href="{burl}cron/daily/?igid={igid}" target="_blank">
                                                <button class="btn-sendforprint btn">
                                                    <span class="glyphicon glyphicon-camera"></span>
                                                </button>
                                                </a>
                                                </div>
                                            </td>
                                        </tr>
                                        {/user}





                                    </tbody>
                                </table>
                            </div>

                        </div></div>

                    <div class="tab-pane" id="orders"> 

                        <div class="form-section form-horizontal">
                            <h2><span class="glyphicon glyphicon-user"></span>Customer Management</h2>





                            <div class="form-body">

                                <table class="table table-striped tb-customer" style="color: #676767" >
                                    <thead>
                                        <tr>
                                            <th>OrderID</th>
                                            <th>Paid</th>
                                            <th>Printed</th>
                                            <th>Count</th>
                                            <th>Price ($)</th>
                                            <th>Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {order}

                                        <tr>
                                            <td><a href="https://{pwmode}dashboard.pwinty.com/Order/Info/{orderid}">{orderid}</a></td>
                                            <td><img src="{burl}style/images/logic/{paid}.png" /></td>
                                            <td><img src="{burl}style/images/logic/{shipped}.png" /></td>
                                            <td>{pic_count}</td>
                                            <td><a href="https://manage.stripe.com/{chmode}/payments/{chid}">{charge_amount}</a></td>
                                            <td>{odate}</td>
                                        </tr>
                                        {/order}





                                    </tbody>
                                </table>
                            </div>

                        </div></div>


                </div>



            </div>

        </div>




    </div>

</div>