    <div id="content_block">
        <div class="first_block">
			<img src="<?php echo site_url('/images/banner.jpg');?>" />
            <div class="first_line" style="display:none"><div class="ten">10</div><div class="text">Instagram Photos Delivered Each Month</div></div>
            <div class="first_line" style="display:none"><span style="font-size: 28px;">Only</span><span style="font-size: 56px; color:#15aaa4; font-family: dali;">$4.99!</span></div>
            
        </div>
        <div class="second_block bg-content-mid">
			<div class="gallery-home">
				<img src="<?php echo site_url('/images/gallery1.png');?>" />
				<img src="<?php echo site_url('/images/gallery2.png');?>" />
				<img src="<?php echo site_url('/images/gallery3.png');?>" />
				<div class="width-100 txtBig">
					We need <strong class="italic">prints</strong>
				</div>
				<img src="<?php echo site_url('/images/gallery4.png');?>" />
				<img src="<?php echo site_url('/images/gallery5.png');?>" />
				<img src="<?php echo site_url('/images/gallery6.png');?>" />
			</div>
            <div class="first_line" style="display:none"><div class="texts">As easy as</div><div class="numbers"><span class="green">1,</span><span class="sky_blue">2,</span><span class="red">3</span>!</div></div>
            <div class="second_line" style="display:none">
                <div class="image_block">
                    <img src="<?php echo site_url('/images/as_easy_as1.jpg');?>" width="278" height="278" title="{title}">
                    <img src="<?php echo site_url('/images/as_easy_as2.jpg');?>" width="278" height="278" title="{title}">
                </div>
                <div class="text_block">
                    <div class="large_text"><div class="tell_us"> Tell us </div><div class="one">1</div></div>
                    
                    where to send<br>
                your Instagram photos
                (home, office, loved ones)
                </div>
            </div>
        </div>
		<div class="third_block bg-green">
			Instagram has your favorite photos
		</div>
        <div class="third_block" style="display:none">
            <div class="images_block">
                <img src="<?php echo site_url('/images/Prioritize1.jpg');?>" width="274" height="274" title="{title}" style="margin-bottom: 4px; margin-right: 4px;">
                <img src="<?php echo site_url('/images/Prioritize2.jpg');?>" width="283" height="283" title="{title}">
            </div>
            <div class="text_block">
                <div class="two">2</div>
                <span class="sky_blue" style="font-size: 51px; margin-left: 55px;">Prioritize</span> your prints:
Most Likes or Most Recent Pics.
<div class="small_text">If you don't take 10 one month,
    we print new ones!</div>
            </div>
        </div>
		<div class="fourth_block txtSmall">
			Let Gramzies print<br />
			them each month<br />
			and deliver them to your door<br />
		</div>
        <div class="fourth_block" style="display:none">
            <div class="text_block">
                <div class="permission">
                    Give Instagram <div class="per_first_line">Permission</div><br>
                    .to share your photos with <span class="green">gramzies</span> for printing
                </div>
                <div class="three">3</div>
            </div>
            <div class="text_block_second">
                <span class="then">Then</span> gramzies grabs 10 photos from your account each month.<br>
                Prints & ships you a pack
            </div>
            <div class="image_block">
                <img src="<?php echo site_url('/images/Instagram_permission.jpg');?>" title="{title}">
            </div>
        </div>
		
		<div class="fifth_block bg-bottom">
			<h2 class="title-1">Automatically each month.</h2>
			<h2 class="title-2">We all want physical prints, forever.</h2>
			<a href="{burl}signup" class="btn-join-now">Join now</a>
		</div>
        <div class="fifth_block" style="display:none">
            <div class="title">Control!</div>
            Tag any of your photos with <span class="sky_blue">#gramzies</span> and it goes to the top of your priority list.
        </div>
        <div class="sixth_box" style="display:none">
            <div class="sixth_block">
              <div class="title">Unlimited Control?</div>
              <div class="text">An Unlimited Option will print every photo you tag with <span class="red">#gramzies</span>, all at once</div>
              <div class="small_text">gramzies takes the photos from Instagram (typically 10 per month). makes professional grade prints, and ships them to you </div>
          </div>
        </div>   
        <div class="seventh_block" style="display:none">
            <div class="text_block">
                <div class="ready"><img src="<?php echo site_url('/images/ready.png');?>" title="Ready"></div>
                <span class="blue">Contact us</span> with any questions or <span class="blue">Sign-up</span> Now.
and get your printed photos from now on.
            </div>
            <div class="images_block">
                <img src="<?php echo site_url('/images/contact_us_home.jpg');?>" title="Contact Us">
            </div>
        </div>
    </div>
