<!DOCTYPE HTML>
<html>
<head>
     <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-45291140-1', 'gramzies.com');
            ga('send', 'pageview');

        </script>
<meta charset="utf-8">
<meta name="viewport" content="initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
<meta name="description" content="{content}">
<meta name="keywords" content="{keywords}">
<title>{title}</title>
 <link rel="stylesheet" type="text/css" href="{burl}js/lightbox/themes/default/jquery.lightbox.css" />
<link rel="stylesheet" href="style/style.css" type="text/css" media="screen" />
<script type="text/javascript" src="{burl}js/jquery-1.10.2.min.js"></script>
 <script type="text/javascript" src="js/theme.js"></script>
 <!-- Javascript Files
        ================================================== -->
        <script type="text/javascript">
            var burl='{mainurl}';
            var stripeKey='{spkey}';
        </script>
        
        <script type="text/javascript" src="{burl}js/jquery.scrollTo.min.js"></script>
        <script type="text/javascript" src="{burl}bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
        <script type="text/javascript" src="{burl}js/main.js?ver=4"></script>
        <script type="text/javascript" src="{burl}js/lightbox/jquery.lightbox.min.js"></script>
         {admin_header}
<link href='http://fonts.googleapis.com/css?family=Josefin+Sans&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
</head>


<body>
<div class="width-100 bg-header">
<div id="main">
 <header>
    <div class="header_block">
        <div class="header_logo">    
            <a href="<?php echo site_url(); ?>" class="logo"></a>
        </div>
		<div class="box-login">
			<a href="{burl}signup" class="active">Join</a>
		</div>
        <div class="menu_icon"></div>
        <nav style="display:none">
            <div class="menu_block">  
              <ul>
                <li><a href="{burl}contact">contact us</a></li>
                <li><a href="{burl}about_gramzies">about gramzies</a></li>
                <li><a href="{burl}signup">sign-up now</a></li>
              </ul>
            </div>
            
            <div class="small_menu_block">          
                  <ul class="small_menu">
                    <li><a href="{burl}contact">contact us</a></li>
                    <li><a href="{burl}about_gramzies">about gramzies</a></li>
                    <li><a href="{burl}signup">sign-up now</a></li>
                  </ul>
            </div>
        </nav>         
    </div>
 </header> 
 </div>
 </div>
 <div class="blue_box">
 </div>
 <div id="main">