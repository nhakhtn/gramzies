<link rel="stylesheet" href="{burl}bootstrap/css/bootstrap.min.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="{burl}js/lightbox/themes/default/jquery.lightbox.css" />
<div id="subheader">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>Sign-up Now</h1>
            </div>
        </div>
    </div>
</div>
<div id="content_block">
    <div class="signup_block">
<div id="content">
    <div class="container" style="position: relative">



        <div class="row">
            <div class="col-md-8 col-sm-12 col-xs-12 col-md-offset-2">
                <form id="signupform" class="">
                    <div id="olduser" style="display: none">{olduser}</div>

                    <div class="form-section form-horizontal ">
                        <h2><span class="glyphicon glyphicon-user"></span>User Information</h2>
                        <div class="form-body">
                                <input type="hidden" value="{token}" class="inpost" id="igtoken" />
                                <input type="hidden" value="{igid}" class="inpost" id="igid" />
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-10">
                                    <input  class="form-control inpost nec email" id="email" name="email" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-section form-horizontal ">
                        <h2><span class="glyphicon glyphicon-shopping-cart"></span>Shipping Information</h2>
                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">First Name</label>
                                <div class="col-sm-10">
                                    <input  class="form-control  inpost nec" id="sfname" name="sfname" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Last Name</label>
                                <div class="col-sm-10">
                                    <input  class="form-control  inpost nec" id="slname" name="slname"  placeholder="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Street Address</label>
                                <div class="col-sm-10">
                                    <input  class="form-control inpost nec" id="saddress" name="saddress"  placeholder="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">City</label>
                                <div class="col-sm-10">
                                    <input  class="form-control inpost nec" id="scity" name="scity" placeholder="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">State</label>
                                <div class="col-sm-10">
                                    <input  class="form-control inpost nec" id="sstate" name="sstate" placeholder="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Zipcode</label>
                                <div class="col-sm-10">
                                    <input  class="form-control  inpost nec numeric" id="szip" name="szip" placeholder="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Country</label>
                                <div class="col-sm-10">
                                    <select  id="scountry" name="scountry"  class="form-control inpost nec">
                                        <option value="US">US</option>
                                        <option value="UK">UK</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-section form-horizontal">
                        <h2><span class="glyphicon glyphicon-credit-card"></span>Billing Information</h2>
                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Card Number</label>
                                <div class="col-sm-10">
                                    <input type="hidden" vlaue="" class="inpost" id="cardtoken" />
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <input type="text" class="form-control nec numeric" maxlength="4" id="card1" name="card1" >
                                        </div>
                                        <div class="col-xs-3">
                                            <input type="text" class="form-control nec numeric" maxlength="4" id="card2" name="card2">
                                        </div>
                                        <div class="col-xs-3">
                                            <input type="text" class="form-control nec numeric" maxlength="4" id="card3" name="card3">
                                        </div>
                                        <div class="col-xs-3">
                                            <input type="text" class="form-control nec numeric"  maxlength="4" id="card4" name="card4">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Card Expiry Date</label>
                                <div class="col-sm-10">

                                    <div class="row">
                                        <div class="col-xs-6 col-sm-9">
                                            <select  id="cardmonth" name="cardmonth"  class="form-control nec">
                                                {month}
                                                <option value="{value}">{text}</option>
                                                {/month}
                                            </select>
                                        </div>
                                        <div class="col-xs-6 col-sm-3">
                                            <select id="cardyear" name="cardyear" class="form-control nec">
                                                {cyear}
                                                <option value="{value}">{text}</option>
                                                {/cyear}
                                            </select>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group" style="border-bottom: 1px dashed #BCBCBC;margin-bottom: 25px;padding-bottom: 30px;">
                                <label class="col-sm-2 control-label">Card CVC</label>
                                <div class="col-sm-10">
                                    <input  class="form-control nec numeric" id="cardcvc" name="cardcvc" placeholder="">
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-2 control-label">First Name</label>
                                <div class="col-sm-10">
                                    <input  class="form-control inpost" id="bfname" name="bfname" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Last Name</label>
                                <div class="col-sm-10">
                                    <input  class="form-control inpost" id="blname" name="blname" placeholder="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Street Address</label>
                                <div class="col-sm-10">
                                    <input  class="form-control inpost" id="baddress" name="baddress" placeholder="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">City</label>
                                <div class="col-sm-10">
                                    <input  class="form-control inpost" id="bcity" name="bcity" placeholder="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">State</label>
                                <div class="col-sm-10">
                                    <input  class="form-control inpost" id="bstate" name="bstate" placeholder="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Zipcode</label>
                                <div class="col-sm-10">
                                    <input  class="form-control inpost numeric" id="bzip" name="bzip" placeholder="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Country</label>
                                <div class="col-sm-10">
                                    <input  class="form-control inpost" id="bcountry" name="bcountry" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-section form-horizontal">
                        <h2><span class="glyphicon glyphicon-cog"></span>Plan Options</h2>

                        <div class="form-body">
                            <div class="form-group" style="display: none">
                                <label class="col-sm-2 control-label">Prints per Month</label>
                                <div class="col-sm-10">
                                    <input  class="form-control inpost nec numeric" placeholder="" value="10" id="ppm" name="ppm">
                                    <span class="help-block">Number of Photos to Print per Month. The default is 10 photos.</span>

                                </div>
                            </div>

                            <div class="form-group" style="border-bottom: 1px dashed #BCBCBC;margin-bottom: 25px;padding-bottom: 20px;">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" class="inpost" value="unlimit" id="unlimit" name="unlimit"> Unlimited Photos Print per Month
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Specific Hashtag</label>
                                <div class="col-sm-10">

                                    <div class="input-group">
                                        <span class="input-group-addon">#</span>
                                        <input type="text" class="form-control inpost" placeholder="" value="{hash}" id="hashtag" name="hashtag" >

                                    </div>
                                    <span class="help-block">The default hashtag is #{hash}</span>

                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <div class="checkbox">
                                        <label>
                                            <input type="radio" class="inpost" value="first" id="first" name="fetchtype" checked="checked"> Print First Photos of Each monthly Cycle
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <div class="checkbox">
                                        <label>
                                            <input type="radio"  class="inpost" value="likes" id="likes" name="fetchtype"> Print Photos with the Most Likes
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-section form-horizontal ">
                            <h2><span class="glyphicon"></span>Coupon</h2>
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Coupon code</label>
                                    <div class="col-sm-10">
                                        <input class="form-control inpost" id="coupon" name="coupon" placeholder="Coupon code" />
                                    </div>
                                </div>
    						</div>
                        </div>
                    </div>
                            
                    <div class="form-submit">
                        <button class="btn btn-primary reset-btn"><span class="glyphicon glyphicon-repeat"></span> Reset</button>
                        <button class="btn btn-default signup-btn"  data-loading-text="Submitting..." id="signup"><span class="glyphicon glyphicon-ok"></span> Sign up</button>
                    </div>
					<script type="text/javascript">
					$("#signup").click(function(){
					  if($("#bfname").val().length > 0 && $("#blname").val().length > 0){
							var todayJSON = new Date().toJSON();
							var today = todayJSON.slice(0,4)+todayJSON.slice(5,7)+todayJSON.slice(8,10);
							var name = $("#bfname").val()+$("#blname").val();

							$.ajax({
								url: "https://www.momsaffiliate.com/track/4074.png?amount=5&order="+name+today
							});
						}
					});	
					</script>

                </form>

            </div>

        </div>




    </div>
</div>
</div>