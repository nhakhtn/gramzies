<div id="content_block" style="display: block; z-index: 1; margin-bottom: -36px;">
    <div class="contact_block">
        <div class="contact_first">
            <img src="<?php echo site_url('/images/contact-page.jpg'); ?>" title="About us">
        </div>
        <div class="contact_box">
            <div class="contact_left_box">
                <div class="contact_right_box">
                    <div class="envloped_box">
                            <div class="envolped_back">                              
                            </div>                        
                            <div class="envloped_top">     
                            </div>   
                             <div class="envolped_middle"> 
                                 <div id="content">
                                      <form id="contactform" class="">
                                        <div class="form-section form-horizontal ">
                                            <h2><span class="glyphicon glyphicon-envelope"></span>The contact form</h2>
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Name</label>
                                                    <div class="col-sm-10">
                                                        <input  class="form-control  inpost nec" id="name" name="name" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Email</label>
                                                    <div class="col-sm-10">
                                                        <input  class="form-control  inpost nec email" id="email" name="email"  placeholder="">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Subject</label>
                                                    <div class="col-sm-10">
                                                        <input  class="form-control inpost nec" id="subject" name="subject"  placeholder="">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Message</label>
                                                    <div class="col-sm-10">
                                                        <textarea style="min-height: 250px" class="form-control inpost nec" name="message" id="message"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                     <div class="form-submit">
                                            <button class="btn btn-default contact-btn"  data-loading-text="Sending..."><span class="glyphicon glyphicon-ok"></span> Send</button>
                                     </div>
                                       
                                    </form>
                                 </div>                               
                            </div>
                            
                    </div>
                </div>
            </div> 
        </div>
    </div>
</div>