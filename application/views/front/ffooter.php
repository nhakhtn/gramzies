    <footer>
        <div id="footer_block">
            <div class="footer_top" style="display:none"></div>
            <div class="footer">
                <div class="footer_logo" style="display:none">
                    <a href="<?php echo site_url();?>" title="Gramzies.com" class="logo">gramizes</a>
                     <div class="rights">All rights reserved</div>
                </div>
                <div class="footer_menu">
                    <ul>
                        <li><a href="{burl}contact">contact us</a></li>
                        <li><a href="{burl}about_gramzies">about gramzies</a></li>
                        <li><a href="{burl}signup">sign-up now</a></li>
                    </ul>
                </div>
				<div class="logo-bottom">
				</div>
                <div class="footer_social" style="display:none">
                     <a href="#" target="_blank" class="twitter"></a>
                    <a href="#" target="_blank" class="facebook"></a>                 
                    <a href="#" target="_blank" class="pintrent"></a>
                </div>
            </div>
        </div>
		<div class="width-100 footer-bottom">
			Gramzies@2014 All rights reserved
		</div>
    </footer>
   
</div>
</body>
</html>