  <div id="content_block">
<div id="subheader">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>Contact us</h1>
                <span>Fill the form to contact us</span>
            </div>
        </div>
    </div>
</div>


<div id="content">
    <div class="container" style="position: relative">



        <div class="row">




            <div class="col-md-8 col-sm-12 col-xs-12 col-md-offset-2">
                <form id="contactform" class="">




                    <div class="form-section form-horizontal ">
                        <h2><span class="glyphicon glyphicon-envelope"></span>The contact form</h2>
                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Name</label>
                                <div class="col-sm-10">
                                    <input  class="form-control  inpost nec" id="name" name="name" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-10">
                                    <input  class="form-control  inpost nec email" id="email" name="email"  placeholder="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Subject</label>
                                <div class="col-sm-10">
                                    <input  class="form-control inpost nec" id="subject" name="subject"  placeholder="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Message</label>
                                <div class="col-sm-10">
                                    <textarea style="min-height: 250px" class="form-control inpost nec" name="message" id="message"></textarea>

                                </div>
                            </div>




                        </div>


                    </div>







                    <div class="form-submit">

                        <button class="btn btn-default contact-btn"  data-loading-text="Sending..."><span class="glyphicon glyphicon-ok"></span> Send</button>

                    </div>


                </form>

            </div>

        </div>




    </div>

</div>
  </div>