<!DOCTYPE html>
<html>
    <head>
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-45291140-1', 'gramzies.com');
            ga('send', 'pageview');

        </script>
        
        <title>{title}</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">


        <!-- CSS Files
        ================================================== -->

        <link rel="stylesheet" href="{burl}bootstrap/css/bootstrap.min.css" type="text/css">


        <link rel="stylesheet" type="text/css" href="{burl}js/lightbox/themes/default/jquery.lightbox.css" />
        <!--[if IE 6]>
        <link rel="stylesheet" type="text/css" href="{burl}js/lightbox/themes/default/jquery.lightbox.ie6.css" />
        <![endif]-->


        <link rel="stylesheet" href="{burl}style/style.css?ver=4" type="text/css">



        <!-- Javascript Files
        ================================================== -->
        <script type="text/javascript">
            var burl='{mainurl}';
            var stripeKey='{spkey}';
        </script>
        <script type="text/javascript" src="{burl}js/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="{burl}js/jquery.scrollTo.min.js"></script>
        <script type="text/javascript" src="{burl}bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
        <script type="text/javascript" src="{burl}js/main.js?ver=4"></script>
        <script type="text/javascript" src="{burl}js/lightbox/jquery.lightbox.min.js"></script>
        {admin_header}


    </head>
    <body>

        <header role="banner" class="navbar navbar-inverse navbar-fixed-top gr-nav">
            <div class="container">
                <div class="navbar-header">
                    <button data-target=".gr-navbar-collapse" data-toggle="collapse" type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{burl}">Gramzies</a>
                </div>
                <nav role="navigation" class="collapse navbar-collapse gr-navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="{burl}">Home</a>
                        </li>
                        <li>
                            <a href="{burl}signup">Signup</a>
                        </li>
                        <li class="">
                            <a href="{burl}contact">Contact</a>
                        </li>


                    </ul>

                </nav>
            </div>
        </header>
