<div id="content_block">
    <div class="about_block">
        <div class="about_first">
			<img src="<?php echo site_url('/images/banner-about.jpg');?>" title="About us">
			<div class="title-main">
				What is Gramzies?
			</div>
			<div class="txtAbout-1">
				Gramzies is your personal Instagram photo printer.<br />
Like an assistant to grab your Instagram posts<br />
print them out, and deliver them to you each month.<br />
You take the pictures and post your favorites to<br />
Instagram.
			</div>
			<div class="txtAbout-2">
				Gramzies takes care of the rest.
			</div>
             <img src="<?php echo site_url('/images/About-page.jpg');?>" title="About us" style="display:none">
        </div>
		<div class="about_second bg-green txtAbout-3">
			How?
		</div>
		<div class="about_third">
			<div class="block_33">
				<div class="circle">
					1
				</div>
				<h3><a href="#">Tell us where to send your Instagram photos</a></h3>
				<div class="txtAbout-4">
					Like your home or office or even a friend (a real human friend).
				</div>
			</div>
			<div class="block_33">
				<div class="circle">
					2
				</div>
				<h3><a href="#">Select an option:</a></h3>
				<div class="txtAbout-4">
					Select Prints Based on Likes. Select Prints Based on Recent Pics
				</div>
			</div>
			<div class="block_33">
				<div class="circle">
					3
				</div>
				<h3><a href="#">Give Instagram permission to share your photos with gramzies for printing</a></h3>
				<div class="txtAbout-4">
					gramzies grabs 10 photos from your account each month.
				</div>
			</div>
			<div class="block_33">
				<div class="circle">
					4
				</div>
				<h3><a href="#">Do you like to take control?</a></h3>
				<div class="txtAbout-4">
					Tag any of your photos with #gramzies and it goes to the top of the priority list
				</div>
			</div>
			<div class="block_33">
				<div class="circle">
					5
				</div>
				<h3><a href="#">Unlimited Control?</a></h3>
				<div class="txtAbout-4">
					An ultimate option will print every photo you tag with #gramzies, all at once.
				</div>
			</div>
			<div class="block_33">
				<img src="<?php echo site_url('/images/icon-about.png');?>" class="img-about"/>
			</div>
		</div>
		<div class="about_fourth bg-black">
			<div class="title-main-2">
				When?
			</div>
			<div class="txtAbout-5">
				Once each month.<br />
				Photos will be process around<br />
				the same day each month.<br />
				We also send you the first pack of photos<br />
				right when you sign up.<br />
			</div>
		</div>
		<div class="about_fifth">
            <div class="text_box">
                <h2 class="title-main-2">
					Why?
				</h2>
				<div class="txtAbout-6">
                Seriously? Do you care about preserving memories? What do you think will last longer...<br>
                images on a digital site or a printed photo in your care.<br>
                Give yourself a wonderful gift each month, a fun pack of your photos.<br><br>
				</div>
            </div>
        </div>
        <div class="about_sixth">
            <div class="text_box">
                 <h2>Who?</h2>
				 <div class="txtAbout-7">
                 This is all about YOU 
                 stop beating yourself up for not printing your photos.<br>
                 You can take care of this once and for all today<br>
                 Instagram... We don't store your login or password. Instagram gives us a token to use<br>
				Payment... We don't store any payment information. <br>
				This is a standard best practice<br>
				Your private information. What is private... everything. We don't sell or share data.<br>
				 <br>
 				<span class="black">Cancel anytime</span><br><br />
				</div>
            </div>
        </div>
		
		
		
		
		
        <div class="about_second" style="display:none">
            <div class="about_second_left">
                <h2>What is Gramzies?</h2>
                <div class="text_box">gramzies is your personal Instagram photo printer.<br> 
                    Like an assistant to grab your Instagram posts<br>
print them out, and deliver them to you each month.<br>
You take the pictures and post your favorites to Instagram.
</div>
                <div class="blue_text_box">gramzies takes care of the rest.</div>
            </div>
            <div class="about_second_right">
                <img src="<?php echo site_url('/images/What-is-Gramzies.jpg');?>" title="What is Gramzies?">
            </div>            
        </div>
        <div class="about_third" style="display:none">
            <div class="how_title"></div>
            <div class="text_box">
                <strong>Tell us where to send your Instagram photos</strong><br>
            Like your home or office or even a friend (a real human friend).<br>
            <br>
            <strong>Select an option:</strong><br>
            Select Prints Based on Likes.<br>
            Select Prints Based on Recent Pics<br>
            <br>
            <strong>Give Instagram permission to share your photos with gramzies for printing</strong><br>
            gramzies grabs 10 photos from your account each month.<br>
            <br>
            <strong>Do you like to take control?</strong><br>
            Tag any of your photos with <span class="blue">#gramzies</span> and it goes to the top of the priority list<br>
            <br>
            <strong>Unlimited Control?</strong><br>
           An ultimate option will print every photo you tag with <span class="blue">#gramzies</span>, all at once.<br>
            </div>
        </div>
        <div class="about_fourth" style="display:none">
            <div class="about_fouth_left">
                <img src="<?php echo site_url('/images/when.jpg');?>" title="When?">
            </div>
            <div class="about_fouth_right">
                <h2>When?</h2>
                Once each month.<br>
                Photos will be process around<br>
                the same day each month.<br>
                We also send you the first pack of photos<br>
                right when you sign up.<br>
                <br>
                <br>
            </div>
        </div>
    </div>
</div>