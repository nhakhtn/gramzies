<?php

class Images extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function IsImageExist($picid, $igid) {
        
      
        
        
        
        $sql = 'SELECT * FROM images WHERE igid=? AND picid=?';
        $igid = (int) $igid;
        
        $query = $this->db->query($sql, escape_arr(array($igid, $picid)));
        if ($query->num_rows() == 1) {
            $image = $query->row_array();
            return $image;
        } else {
            return false;
        }
    }

    function ImageCount($igid) {
        $sql = 'SELECT count(id) as cnt FROM images WHERE igid=?';
        $igid = (int) $igid;


        $query = $this->db->query($sql, escape_arr(array($igid)));
        if ($query->num_rows() == 1) {
            $image = $query->row_array();
            return $image['cnt'];
        } else {
            return 0;
        }
    }

    function AlreadyPrinted($igid, $limit) {
        $sql = "SELECT picid,igid,link,src, true as indb FROM images WHERE igid=? limit " . $limit;
        $igid = (int) $igid;
        $query = $this->db->query($sql, escape_arr(array($igid)));
        return $query->result_array();
    }

    function InsertImage($image) {
        
        if (!$image['indb']) {

            $pdate = date("Y-m-d");
            $image['pdate']=$pdate;
            unset($image['indb']);
            $this->db->insert('images', escape_arr($image));
            
            
            
        } else {
            return false;
        }

        return true;
    }

}