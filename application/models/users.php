<?php

class Users extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function GetUsers() {
        $sql = 'SELECT * FROM users';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
	
    function GetUsersBySdate($sdate) {
        $sql = "SELECT * FROM users WHERE sdate='" . $sdate . "' AND `status` = 'success'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
    function GetUserByIgid($igid) {
        $sql = "SELECT * FROM users WHERE igid = " . $igid ;
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
     function GetUsersByRandom($limit) {
        $sql = "SELECT * FROM users order by RAND() limit " . $limit ;
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    

    function DeleteUser($igid) {
        $id = (int) $igid;
        $this->db->where('igid', $id);
        $this->db->delete('users');
    }

    function IsUserExist($igid) {
        $sql = 'SELECT * FROM users WHERE igid=?';
        $id = (int) $igid;
        $query = $this->db->query($sql, escape_arr(array($id)));
        if ($query->num_rows() == 1) {
            $user = $query->row_array();
            return $user;
        } else {
            return false;
        }
    }

    function UpdateAfterOrder($shipcount, $igid) {
        $id = (int) $igid;

        $this->db->where('igid', $id);

        $cdate = date("Y-m-d H:i");
        $sdate = date("Y-m-d", strtotime("+31 days"));

        $this->db->set('shipped', 'shipped + ' . (int) $shipcount,false);
        
        $data = array(
            
            'sdate' => $sdate,
            'lastship' => $cdate
        );

        $this->db->update('users', $data);
    }
    
    function UpdateStatus($result, $igid) {
        $id = (int) $igid;

        $this->db->where('igid', $id);
        $data = array(
            'lastrun' => $result['lastrun'],
            'status' => $result['status'],
            'reason' => $result['reason']
        );

        $this->db->update('users', $data);
    }

    function UpdateCardID($cardid, $igid) {
        $id = (int) $igid;

        $this->db->where('igid', $id);
        $this->db->update('users', escape_arr(array('cardid' => $cardid)));
    }

    function UpdateCredit($free, $off, $igid) {
        $id = (int) $igid;

        $this->db->where('igid', $id);
        $this->db->update('users', escape_arr(array('freecredit' => $free, 'offcredit' => $off)));
    }
    
    function UpdateSignup($fields) {

        $id = (int) $fields['igid'];

        $this->db->where('igid', $id);
        $this->db->update('users', escape_arr($fields));
    }

    function UpdateCoupon($coupon, $igid) {
        $id = (int) $igid;

        $this->db->where('igid', $id);
        $this->db->update('users', escape_arr(array('coupon' => $coupon)));
    }

    function Signup($fields) {

        $igid = (int) $fields['igid'];
        

        $cdate = date("Y-m-d H:i");
        $sdate = date("Y-m-d", strtotime("+1 days"));


        $fields['cdate'] = $cdate;
        $fields['sdate'] = $sdate;
		
        $user = $this->IsUserExist($igid);


        if ($user) {
            $this->UpdateSignup($fields);
        } else {
            $this->db->insert('users', escape_arr($fields));
            $last_id = $this->db->insert_id();
            $this->session->set_userdata('last_id', $last_id); 
        }
    }
      
    function insert_user_details($fields) {
        $cdate = date("Y-m-d H:i");
        $sdate = date("Y-m-d", strtotime("+1 days"));
	
        $fields['cdate'] = $cdate;
        $fields['sdate'] = $sdate;
        
        if(isset($fields['coupon'])) {
            $coupon = getoption('coupon');
            $coupons = json_decode($coupon);
            if (isset($coupons->$fields['coupon'])) {
                $fields['coupon'] = $coupons->$fields['coupon'];
            } else {
                $fields['coupon'] = 0;
            }
        } else {
            $fields['coupon'] = 0;
        }
          
        $this->db->insert('users', escape_arr($fields));
        $last_id = $this->db->insert_id();
        $this->session->set_userdata('last_id', $last_id); 
       
    }
      
        /* mi */
    function update_user_instagram($fields){
         //$id = $_SESSION['last_id'];
         $id  = $this->session->userdata('last_id'); 

        $this->db->where('id', $id);
        $this->db->update('users', escape_arr($fields));
    }
  
}