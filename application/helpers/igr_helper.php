<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
?>
<?php

function getImages($user) {
    $CI = &get_instance();

    // INI THE USER's RULES
    $maxpage = getoption('maxpage', 0);
    if ($maxpage == 0) {
        $maxpage = 500000;
    }
    $unlimited = $user['unlimit'];

    if ($unlimited) {
        $count = 500000;
    } else {
        $count = $user['ppm'];
    }

    $tag = $user['hashtag'];


    $igid = $user['igid'];
    $fetchtype = $user['first'] * 1 + $user['likes'] * 2;





    // Geting Images

    $instagram = new Instagram(array(
                'apiKey' => getoption('ig_key'),
                'apiSecret' => getoption('ig_secret'),
                'apiCallback' => base_url() . INDEX . 'signup/callback'
            ));
    $instagram->setAccessToken($user['igtoken']);





    try {
        $images = $instagram->getUserMedia($igid, 100);
    } catch (Exception $exc) {

        $return = array('paid' => 0, 'shipped' => 0, 'charge_amount' => 0, 'orderid' => '', 'chid' => '', 'chmode' => (getoption('op_stipe_test') ? 'test' : 'live'), 'pwmode' => (getoption('op_pw_test') ? 'sandbox' : ''), 'igid' => $user['igid'], 'pic_count' => 0, 'pics' => '', 'error' => true, 'message' => $exc->getMessage());

        return $return;
    }



    $pics = array();
    $pics_all = array();


    $pcount = 0;

    if (isset($images->data)) {



        list($pics, $pics_all) = GetPageByTag($images->data, $tag, $igid, $count);
        $pcount = count($pics);








        $pages = 1;
        $p = $images->pagination;


        while ($p && $pages <= $maxpage && $pcount < $count) {

            try {
                $images = $instagram->pagination($images);
            } catch (Exception $exc) {

                $return = array('paid' => 0, 'shipped' => 0, 'charge_amount' => 0, 'orderid' => '', 'chid' => '', 'chmode' => (getoption('op_stipe_test') ? 'test' : 'live'), 'pwmode' => (getoption('op_pw_test') ? 'sandbox' : ''), 'igid' => $user['igid'], 'pic_count' => 0, 'pics' => '', 'error' => true, 'message' => $exc->getMessage());

                return $return;
            }



            if ($images) {



                list($ppics, $ppics_all) = GetPageByTag($images->data, $tag, $igid, $count - $pcount);

                $pics = array_merge($pics, $ppics);
                $pics_all = array_merge($pics_all, $ppics_all);

                $pcount = count($pics);


                $p = $images->pagination;
                $pages++;
            } else {
                $p = NULL;
            }
        }
    }

    // First Stage From IG
    if ($unlimited) {
        $count = getoption('offcount');
    }


    if ($pcount < $count) { // We don't have enough Tags!!!
        $limit = $count - $pcount;
        if ($limit > count($pics_all))
            $limit = count($pics_all);


        if ($fetchtype == 2) {
            usort($pics_all, 'compare_likes');
        }

        $pics_all = array_slice($pics_all, 0, $limit);
        $pics = array_merge($pics, $pics_all);

        $pcount = count($pics);
    }

    // Second Stage From DB

    $return = array();

    if ($pcount < $count) { // we don't have enough pics in IG, we have several options
        // 1. pcount==0 ======> Email User (Don't print)
        // 2. pcount>0  ======> Do these:
        // 2.a. Does user have aleady printed on our DB?
        // 2.a:Yes ===========> Choose from aleady printed ones (print)
        // 2.a:No  ===========> Email User (print)
        if ($pcount > 0) {

            $ucount = $CI->images->ImageCount($igid);


            //  Does user have aleady printed on our DB? 
            if ($ucount > 0) { // : Yes
                $limit = $count - $pcount; // We want $limit more image

                $dbpic = $CI->images->AlreadyPrinted($igid, $limit);
                $pics = array_merge($pics, $dbpic);


                if ($limit > $ucount) { // We dont have enough aleady printed...
                    // Print, Email Error
                }

                $return = SendForPrint($pics, $user);
            } else { // NO
                // Print, Email Error
                $return = SendForPrint($pics, $user);
            }
        } else {
            // Do not print, Email Error
            $return = array('paid' => 0, 'shipped' => 0, 'charge_amount' => 0, 'orderid' => '', 'chid' => '', 'chmode' => (getoption('op_stipe_test') ? 'test' : 'live'), 'pwmode' => (getoption('op_pw_test') ? 'sandbox' : ''), 'igid' => $user['igid'], 'pic_count' => 0, 'pics' => '', 'error' => true, 'message' => 'No new tagged/first/liked pictures for print this month. Email is sent');
        }
    } else {
        $return = SendForPrint($pics, $user);
    }
    return $return;
}

function GetPageFirst($images, $igid, $limit) {
    $CI = &get_instance();

    $pics = array();
    $pcount = 0;

    foreach ($images as $img) {

        if ($pcount < $limit) {

            list($picid, $pic_igid) = explode('_', $img->id);

            if (!$CI->images->IsImageExist($picid, $igid)) {

                $pics[] = array(
                    'picid' => $picid,
                    'igid' => $igid,
                    'link' => $img->link,
                    'src' => $img->images->standard_resolution->url,
                    'indb' => false
                );

                $pcount+=1;
            }
        }
    }

    return $pics;
}

function GetPageByTag($images, $tag, $igid, $limit) {
    $CI = &get_instance();

    $pics = array();
    $pics_all = array();

    $pcount = 0;

    foreach ($images as $img) {

        list($picid, $pic_igid) = explode('_', $img->id);


        if (in_array($tag, $img->tags)) {
            if ($pcount < $limit) {

                if (!$CI->images->IsImageExist($picid, $igid)) {

                    $pics[] = array(
                        'picid' => $picid,
                        'igid' => $igid,
                        'link' => $img->link,
                        'src' => $img->images->standard_resolution->url,
                        'likes' => $img->likes->count,
                        'indb' => false
                    );

                    $pcount+=1;
                }
            }
        } else {
            if (!$CI->images->IsImageExist($picid, $igid)) {
                $pics_all[] = array(
                    'picid' => $picid,
                    'igid' => $igid,
                    'link' => $img->link,
                    'src' => $img->images->standard_resolution->url,
                    'likes' => $img->likes->count,
                    'indb' => false
                );
            }
        }
    }

    return array($pics, $pics_all);
}

function compare_likes($a, $b) {

    return strnatcmp($b['likes'], $a['likes']);
}

function CalculateCost($piccount, $user) {
    $cost = 0;

    $offprice = getoption('offprice', 0.88);
    $extraprice = getoption('extraprice', 1);

    $offcount = getoption('offnumber', 10);

    $freecount = getoption('freecount', 0);

    $user_freecount = $user['freecredit'];
    $user_offcount = $user['offcredit'];

    $updatecredit = false;




    if ($piccount <= $offcount) {
        $cost = $piccount * $offprice;
    } else {
        $cost = $offcount * $offprice;
        $diff = $piccount - $offcount;



        // 1. Firt "total off"
        // 2. Then "total free"

        $diff = ($diff - $freecount < 0) ? 0 : $diff - $freecount;


        // 3. Then "user free"
        // 4. Then "user off"

        $diff = ($diff - $user_freecount < 0) ? 0 : $diff - $user_freecount;
        $new_u_free = ($diff - $user_freecount < 0) ? $user_freecount - $diff : 0;

        $new_u_off = $user_offcount;


        if ($diff <= $user_offcount) {
            $cost+=$diff * $offprice;
            $new_u_off = $user_offcount - $diff;
        } else {
            $cost+=$user_offcount * $offprice;
            $new_u_off = 0;
            $diff = $diff - $user_offcount;



            $cost+=$diff * $extraprice;
        }
        $CI = &get_instance();
        $CI->users->UpdateCredit($new_u_free, $new_u_off, $user['igid']);
    }

    return $cost;
}

function SendForPrint($pics, $user) {
    $CI = &get_instance();

    $cost = CalculateCost(count($pics), $user);
    
    $charge_in_card = 0;
    if(isset($user['coupon']) && $user['coupon'] > 0) {
        $charge_in_card = $user['coupon'] - $cost;
        if($charge_in_card < 0) {
            $CI->users->UpdateCoupon(0, $user['igid']);    
            $card = ChargeCard(abs($charge_in_card), $user);
        }
        else {
            //update user
            $CI->users->UpdateCoupon($charge_in_card, $user['igid']);    
            $card = (object) array('paid' => $cost, 'id' => 0);
            $card = json_encode($card);
        }
    } else {
        $card = ChargeCard($cost, $user);
    }
    $return = array('paid' => 0, 'shipped' => 0, 'charge_amount' => $cost, 'orderid' => '', 'chid' => '', 'chmode' => (getoption('op_stipe_test') ? 'test' : 'live'), 'pwmode' => (getoption('op_pw_test') ? 'sandbox' : ''), 'igid' => $user['igid'], 'pic_count' => count($pics), 'pics' => '', 'error' => false, 'message' => '');
    $jcard = json_decode($card);
    $sdate = date("Y-m-d");
    if(isset($jcard->error) && $jcard->error == true) {
        //store to db
        // User is not charged
        $CI->users->UpdateStatus(array('lastrun' => $sdate, 'status' => 'fail', 'reason' => $jcard->message), $user['igid']);
        //email to admin:
        SendMail('nhakhtn@gmail.com', 'There is an error with a card - gid:' . $user['igid'], $jcard->message);
        $return['error'] = true;
        $return['message'] = $jcard->message;
        return $return;
    }
    
    if (!isset($jcard->paid)) {

        $return['error'] = true;
        $return['message'] = 'User is not charged.';
        return $return;
    }

    $return['paid'] = 1;
    $return['chid'] = $jcard->id;


    //store to db
    // User is charged
    $CI->users->UpdateStatus(array('lastrun' => $sdate, 'status' => 'success', 'reason' => ''), $user['igid']);

    $order = SendPhotoOrder($pics, $user);


    if (isset($order['error'])) {
        // Not sent for prtint
        $return['error'] = true;
        $return['message'] = $order['message'];

        return $return;
    }

    $return['orderid'] = $order;
    $return['shipped'] = 1;
    $pics_db = '';

    foreach ($pics as $image) {
        $CI->images->InsertImage($image);
        $pics_db.=$image['link'] . '|';
    }
    if ($pics_db) {
        $pics_db = substr($pics_db, 0, -1);
    }

    $CI->users->UpdateAfterOrder(count($pics), $user['igid']);

    $CI->orders->InsertOrder($pics_db, $return);

    return $return;
}

function SendPhotoOrder($pics, $user) {
    $CI = &get_instance();
    $CI->load->library('pwinty');

    $pwinty = new PHPPwinty(getoption('pw_merchant'), getoption('pw_key'), getoption('pw_test'));






    $order = $pwinty->createOrder($user['sfname'] . ' ' . $user['slname'], $user['saddress'], "", $user['scity'], $user['sstate'], $user['szip'], $user['scountry'], "Photos by " . $user['igid']);


    if (!$order) {
        SendMail(getoption('adminmail'), 'The order is not submitted to Pwinty', "Order: $order <br> Section: Pwinty <br> User: " . $user['igid']);
        $return = array('error' => true, 'message' => $pwinty->last_error);
        return $return;
    }



    foreach ($pics as $image) {
        $photo2 = $pwinty->addPhoto($order, "4x4", $image['src'], "1", "Crop");
        if (!$photo2) {
            SendMail(getoption('adminmail'), 'The picture is not submitted to Pwinty', "Order: $order <br> Section: Pwinty <br> User: " . $user['igid']);
            $return = array('error' => true, 'message' => $pwinty->last_error);
            return $return;
        }
    }


    $result = $pwinty->updateOrderStatus($order, "Submitted");

    if (!$result) {
        SendMail(getoption('adminmail'), 'The status is not submitted to Pwinty', "Order: $order <br> Section: Pwinty <br> User: " . $user['igid']);
        if (!$photo2) {
            $return = array('error' => true, 'message' => $pwinty->last_error);
            return $return;
        }
    }

    return $order;
}

function ChargeCard($amount, $user) {
    $CI = &get_instance();
    $CI->load->library('stripelib');

    Stripe::setApiKey((getoption('stipe_test')) ? getoption('stipe_s') : getoption('stipe_s_live'));

    $igid = $user['igid'];
    $cid = $user['cardid'];
    $ctoken = $user['cardtoken'];

    if (!$cid) {

        $customer = Stripe_Customer::create(array(
                    "card" => $ctoken,
                    "description" => $user['email'])
        );

        $cid = $customer->id;

        $CI->users->UpdateCardID($cid, $user['igid']);
    }
    $result = json_encode(array());


    try {

        $result = Stripe_Charge::create(array(
                    "amount" => intval($amount * 100),
                    "currency" => "usd",
                    "customer" => $cid)
        );
    } catch (Stripe_AuthenticationError $e) {

        $result = array('error' => true, 'message' => $e->getMessage());
        SendMail(getoption('adminmail'), 'There is an error with Stripe.', $e->getMessage() . "<br> Section: ApiConnection <br> User:   $igid");
    } catch (Stripe_ApiConnectionError $e) {
        // Network problem, perhaps try again.
        $result = array('error' => true, 'message' => $e->getMessage());
        SendMail(getoption('adminmail'), 'There is an error with Stripe.', $e->getMessage() . "<br> Section: ApiConnection <br> User:   $igid");

        $result = json_encode($result);
    } catch (Stripe_InvalidRequestError $e) {
        // You screwed up in your programming. Shouldn't happen!
        $result = array('error' => true, 'message' => $e->getMessage());
        SendMail(getoption('adminmail'), 'There is an error with Stripe.', $e->getMessage() . "<br> Section: InvalidRequest <br> User:   $igid");

        $result = json_encode($result);
    } catch (Stripe_ApiError $e) {
        // Stripe's servers are down!
        $result = array('error' => true, 'message' => $e->getMessage());
        SendMail(getoption('adminmail'), 'There is an error with Stripe.', $e->getMessage() . "<br> Section: Api <br> User:   $igid");
        $result = json_encode($result);
    } catch (Stripe_CardError $e) {
        // Card was declined.
        $e_json = $e->getJsonBody();
        $error = $e_json['error'];


        $result = array('error' => true, 'message' => $error['message']);
        SendMail(getoption('adminmail'), 'There is an error with Stripe.', $e->getMessage() . "<br> Section: Card <br> User:   $igid");

        $result = json_encode($result);

        SendMail($user['email'], 'There is an error with your card.', $error['message']);
    }

    return $result;
}
