<?php //

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
?>
<?php

function ShowSignup($user=array() , $token = 'NUll') {
    session_start();
    $CI = &get_instance();

    $username='';
    //$username = $user->username;
    
    $name = '';

     $bio = '';
    $mcount = '';
    $uid = '';
    $img = '';
    //var_dump($_SESSION['post_data']);
  
    if(isset($_SESSION['post_data'])){
                $uid = $user->id;
                $img = $user->profile_picture;
                $name = $user->full_name;
                $bio = $user->bio;
                $mcount = $user->counts->media;
                $username = $user->username;
    }


    $c_year = array();
    $b_year = array();

    $month = array();
    $days = array();


    $monthholder = array(
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December');

    $years = date('Y');
    while ($years <= '10' + date('Y')) {
        $c_year[] = array('value' => $years, 'text' => $years);
        $years++;
    }

    $years = date('Y') - 93;
    while ($years <= date('Y')) {
        $b_year[] = array('value' => $years, 'text' => $years);
        $years++;
    }

    $d = 1;
    while ($d <= 31) {
        $days[] = array('value' => str_pad($d, 2, '0', STR_PAD_LEFT), 'text' => str_pad($d, 2, '0', STR_PAD_LEFT));
        $d++;
    }


    $m = 0;
    while ($m <= 11) {
        $month[] = array('value' => str_pad($m + 1, 2, '0', STR_PAD_LEFT), 'text' => $monthholder[$m]);
        $m++;
    }

    $olduser = $CI->users->IsUserExist($uid);

    /*
    if ($olduser) {
        $olduser = json_encode($olduser);
    } else {
        $olduser = '';
    }
    */



    $data = array(
        'username' => $username,
        'name' => $name,
        'mcount' => $mcount,
        'igid' => $uid,
        'img' => $img,
        'token' => $token,
        'spkey' => (getoption('stipe_test')) ? getoption('stipe_p') : getoption('stipe_p_live'),
        'cyear' => $c_year,
        'byear' => $b_year,
        'month' => $month,
        'days' => $days,
        'olduser' => $olduser
    );
    
    $content = "10 Instagram prints delivered each month for only $4.99. Gramzies grabs 10 each month based on likes or recent photos. Automatic delivery of your memories.";
    $keyword = '';
    $data = GetHeader('Gramzies Monthly Print Sign-up',$content,$keyword,$data);

    

    $CI->parser->parse('front/fheader', $data);
    $CI->parser->parse('front/fsignup', $data);
    $CI->parser->parse('front/ffooter', $data);
}

function SendMail($to, $subject, $message, $from_e = '', $from_n = '') {
    $CI = &get_instance();
    $from_mail = $CI->config->item('from_mail');
    $from_name = $CI->config->item('from_name');

    if ($CI->config->item('gmail')) {
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => $CI->config->item('gmail_mail'),
            'smtp_pass' => $CI->config->item('gmail_pass'),
            'mailtype' => 'html'
        );
        $CI->load->library('email', $config);
        $CI->email->set_newline("\r\n");
        $from_name = '';
        $from_mail = $CI->config->item('gmail_mail');
    } else {
        $config = Array(
            'mailtype' => 'html'
        );
        
        $config['protocol'] = 'sendmail';
        
        
        $CI->load->library('email', $config);
    }


    if ($from_e) {

        $CI->email->reply_to($from_e, $from_n);
        $message = '<br>From: ' . $from_e . '<br><br>Message:<br>' . $message;
    }



    $message = "<h1 style=\"color: rgb(91, 91, 91); font-size: 16px; border-top: 1px dashed rgb(167, 167, 167); padding-top: 30px;\">$subject</h1><p style=\"padding-bottom: 30px;\">$message</p>";
    $data = array(
        'message' => $message,
        'SITE_NAME' => getoption('sitename'),
        'SITE_SLOGAN' => getoption('title')
    );



    $message = $CI->parser->parse('email/main.html', $data, true);



    $CI->email->from($from_mail, $from_name);
    $CI->email->to($to);


    $CI->email->subject($subject);
    $CI->email->message($message);

    $CI->email->send();
}

function getoption($name, $default = '') {
    $CI = &get_instance();

    if (strrpos($name, 'op_') === false) {
        $name = 'op_' . $name;
    }


    if (array_key_exists($name, $CI->options)) {
        $value = $CI->options[$name];
        return get_numeric($value);
    } else {
        return $default;
    }
}

function setoption($name, $value) {
    $CI = &get_instance();

    if (strrpos($name, 'op_') === false) {
        $name = 'op_' . $name;
    }


    $config_data = array(
        's_name' => $name,
        's_value' => $value
    );


    $CI->db->where('s_name', $name);

    $CI->options[$name] = $value;

    if ($CI->db->count_all_results('setting') > 0) {
        $CI->db->where('s_name', $name);
        return $CI->db->update('setting', $config_data);
    } else {
        $CI->db->where('s_name', $name);
        return $CI->db->insert('setting', $config_data);
    }
}

function get_numeric($val) {
    if (is_numeric($val)) {
        return $val + 0;
    }
    return $val;
}

function escape_arr($array) {
    $new_arr = array();

    if (is_array($array)) {
        foreach ($array as $key => $value) {
            if (is_int($value)) {
                $new_arr[$key] = (int) addslashes($value);
            } else {
                $new_arr[$key] = addslashes($value);
            }
        }
        return $new_arr;
    }
}

function CheckLoggedIn() {
    $CI = &get_instance();
    $CI->load->library("auth");
    $id = $CI->auth->loggedin();

    if (!$id) {
        //$CI->load->helper('url');
        //redirect('/iradmin/login');
        return false;
    }
    return $id;
}

function IsAdmin() {
    $CI = &get_instance();

    $id = CheckLoggedIn();

    if (!$id) {
        return false;
    }

    $CI->load->model("main");
    $admin = $CI->main->IsAdmin($id);
    return $admin;
}

function GetHeader($title = '', $content = '',$keywords = '', $pagedata, $admin = false) {

    $admin_header = '';
    if ($admin) {
        $admin_header = '<script type="text/javascript" src="' . base_url() . 'js/admin.js?ver=4"></script>';
    }


    $data = array(
       // 'title' => getoption('title') . ' | ' . $title,
        'title' => $title,
        'burl' => base_url(),
        'mainurl' => base_url() . INDEX,
        'admin_header' => $admin_header,
        'hash' => getoption('hash'),
        'content' => $content,
        'keywords' =>$keywords
    );

    $data = array_merge($data, $pagedata);

    return $data;
}

function formatMoney($number, $fractional = false) {
    if ($fractional) {
        $number = sprintf('%.2f', $number);
    }
    while (true) {
        $replaced = preg_replace('/(-?\d+)(\d\d\d)/', '$1,$2', $number);
        if ($replaced != $number) {
            $number = $replaced;
        } else {
            break;
        }
    }
    return $number;
}

