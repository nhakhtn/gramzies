<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function load_setting() {
    
    $CI = &get_instance();
 
    $query = $CI->db->get('setting');

// loop through the results
    foreach ($query->result_array() as $row) {
        $CI->options[$row['s_name']]=$row['s_value'];
        
        
    }
}

?>
