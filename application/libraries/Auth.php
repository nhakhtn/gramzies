<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Auth {

    var $secret_key = 'qkmH5gaYPs35PD6xfG8Kq3Dd46N1mz32';
    var $cookie_name = 'U4OcJsTqtv';
    var $cookie_long = 15552000;
    var $cookie_short = 0;
    var $cookie_path = '/';
    var $cookie_domain = '';

    //var $loggedin = false;

    public function __construct() {
        $this->CI = &get_instance();
        $this->loggedin = $this->verify_cookie();
    }

    public function loggedin() {
        return $this->loggedin;
    }

    public function logout() {
        $this->loggedin = false;
        $this->delete_cookie();
    }

    public function userid() {
        $content = $this->CI->input->cookie($this->cookie_name);

        if ($content) {
            list($id, $token) = explode('|', $content);
            return $id;
        } else
            return 0;
    }

    private function generate_token($id) {
        $key = hash_hmac('sha512', $id, $this->secret_key);
        $token = hash_hmac('sha512', $id, $key);

        return $token;
    }

    private function verify_cookie() {
        $content = $this->CI->input->cookie($this->cookie_name);

        if ($content) {
            list($id, $token) = explode('|', $content);
            $hmac = $this->generate_token($id);

            if ($hmac == $token) {
                return $id;
            }
        }
        return false;
    }

    private function create_cookie($id, $remember = false) {
        //$this->CI->load->helper('cookie');
        $cookie = array('name' => $this->cookie_name, 'value' => $id . '|' . $this->
                    generate_token($id), 'domain' => $this->cookie_domain, 'path' => $this->
            cookie_path);

        if ($remember)
            $cookie['expire'] = $this->cookie_long;
        else
            $cookie['expire'] = $this->cookie_short;

        set_cookie($cookie);
    }

    private function delete_cookie() {
        $this->CI->load->helper('cookie');
        $cookie = array('name' => $this->cookie_name, 'value' => '', 'expire' => '',
            'domain' => $this->cookie_domain, 'path' => $this->cookie_path);
        set_cookie($cookie);
    }

    public function authenticate($username, $password, $remember = false) {

        if ($password == getoption('adminpass') && $username == getoption('adminmail')) {
            $this->create_cookie($username, $remember);
            return true;
        }

        return false;
    }

    public function password_hash($password) {
        return hash_hmac("sha512", "salt" . $password, $this->secret_key);
    }

}

/* End of file Someclass.php */
