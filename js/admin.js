

jQuery(document).ready(function(){
  
  
    $('input[type=checkbox]').each(function(){
        if ($(this).attr('rel')=='1'){
            $(this).attr('checked','checked');
        }else{
            $(this).removeAttr('checked');
        }
    })
        
        
    $('.btn-edit').click(function(e){
        e.preventDefault();
        var efree=$(this).parents('tr').find('.efree');
        var eoff=$(this).parents('tr').find('.eoff');
        var igid=$(this).attr('rel');
         
            
        if ($(this).hasClass('btn-apply')){
            $(efree).removeAttr('contenteditable');
            $(eoff).removeAttr('contenteditable');
          
            $(efree).removeClass('editbox');
            $(eoff).removeClass('editbox');
          
            $(this).removeClass('btn-apply');
            
            var post={};
            post['free']=$(efree).text();
            post['off']=$(eoff).text();
            post['igid']=igid;
            
            ajaxify('updatecredit', post, function(data){
                
                }, function(){
                
                });
            
        }else{
           
      
            $(efree).attr('contenteditable', '');
            $(eoff).attr('contenteditable', '');
      
            $(efree).addClass('editbox');
            $(eoff).addClass('editbox');
      
      
            $(efree).focus();
            $(this).addClass('btn-apply');
             
        }
      
      
      
      
    })
    
    
    $('.btn-del').click(function(e){
        e.preventDefault();
        var igid=$(this).attr('rel');
        
        var post={};
         
        post['igid']=igid;
            
        $(this).parents('tr').remove();
        
        ajaxify('deleteuser', post, function(data){
                
            }, function(){
                
            });
        
    })
    
    
    $('.option-btn').click(function(e){
        e.preventDefault();
        
        $(this).button('loading');
        
        
        
        if (!Verify($(this).parents('.tab-pane'))){
            $(this).button('reset');
            return false;
        }
        
        
        var post={};
        post['fields']=GetFields($(this).parents('.tab-pane'));
        
        
        
        ajaxify('updateoption', post, function(){
            
            ShowSucc('Updated!', 'The site options are updated');
            
          
            
            
            $('.option-btn').button('reset');
        }, function(){
            
            $('.option-btn').button('reset');
        });
        
        
        
    })
    
    
    $('.login-btn').click(function(e){
        e.preventDefault();
        
        $(this).button('loading');
        
        
        
        if (!Verify($(this).parents('.tab-pane'))){
            $(this).button('reset');
            return false;
        }
        
        
        var post={};
        post['fields']=GetFields();
        
        
        
        ajaxify('login', post, function(){
            
            ShowSucc('You are in!', ' You will be redirected to the admin panel.');
            
            setTimeout(function(){
                location.href=burl+'admin';
            }, 2000);
            
            
            
            $('.login-btn').button('reset');
        }, function(){
            
            ShowError('username', ' Wrong username or password');
            $('.login-btn').button('reset');
        });
        
        
        
    })
    
})


