var ajaxurl = burl + 'ajax/';
var insta_url = burl + 'signup/instagram';
var errorT=null;



jQuery(document).ready(function(){
    
    $('#bytag').prop('checked', true);
    
    ActivateNumeric();
    LoadOldUser();
    
    
    $('input[name=fetchtype]').change(function(e){
      
        
        if ($('#bytag').is(':checked')){
            $('#hashtag').removeAttr('disabled');
            
        }else{
            $('#hashtag').attr('disabled','disabled');
        }
        
        
    })
    
    if ($('#bytag').is(':checked')){
        $('#hashtag').removeAttr('disabled');
            
    }else{
        $('#hashtag').attr('disabled','disabled');
    }
        
        
    $('#unlimit').change(function(e){
      
        
        if ($('#unlimit').is(':checked')){
            $('#ppm').attr('disabled','disabled');
            
            
        }else{
            $('#ppm').removeAttr('disabled');
        }
        
        
    })
    
    if ($('#unlimit').is(':checked')){
        $('#ppm').attr('disabled','disabled');
            
            
    }else{
        $('#ppm').removeAttr('disabled');
    }
        
    $('.signup-btn').click(function(e){
        e.preventDefault();
        $(this).button('loading');
        
        
        
        
        if (!Verify()){
            $(this).button('reset');
            return false;
        }
       
       
        Stripe.setPublishableKey(stripeKey);
        
        var cardnum=$('#card1').val() + $('#card2').val() + $('#card3').val() + $('#card4').val();
        var cvc=$('#cardcvc').val();
        var exp_month=$('#cardmonth').val();
        var exp_year=$('#cardyear').val();
        
        
        Stripe.card.createToken({
            number: cardnum,
            cvc: cvc,
            exp_month: exp_month,
            exp_year: exp_year
        }, stripeResponseHandler);
    
    })
    
    
    $('.contact-btn').click(function(e){
        e.preventDefault();
        $(this).button('loading');
       
        
        
        
        if (!Verify()){
            $(this).button('reset');
            return false;
        }
        
        var post={};
        post['fields']=GetFields();
        
        ajaxify('contact', post, function(){
            
            ShowSucc('Thank you!', 'Your message is sent.');
            $('.contact-btn').button('reset');
            setTimeout(function(){
                location.href=burl;
            }, 4000);
            
        }, function(){
            
            $('.contact-btn').button('reset');
        });
        
        
        
        
    })

})

$(window).resize(function(){
    HomeResize();
    
})

$(window).load(function(){
    HomeResize();

})

function HomeResize(){
    if ($('.homebtn').length==0){
        return false;
    }
    
    var nav_h=$('.navbar').height();
    var sub_h=$('#subheader').outerHeight(true);
    var window_h=$(window).height();
    
    $('#slider').css('top',nav_h);
    $('#slider').height(window_h-nav_h-sub_h);
    
    var ftop=100;
    if ($(window).width()<600){
        ftop=70;
    }
    $('.content-box span').each(function(){
        
        $(this).css('top',ftop);
        
        ftop+=$(this).outerHeight(true)+10;
        
    })
    
    
}

function ShowError(id,message){
    if ($('.gerror').length>0)
    {
        $('.gerror').remove();
    }
    if (errorT)
    {
        window.clearTimeout(errorT);
    }
    $('.has-error').removeClass('has-error');
    var p=$('#' + id).parents('.form-group');

    $(p).addClass('has-error');
    
    $.scrollTo($(p),{
        offset:-200
    });
    
    
    $('#content').append('<div class="alert alert-danger alert-dismissable gerror"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Error!</strong> <span></span></div>');
    
    var subvisible=isScrolledIntoView($('#subheader'));
    
    var nav_h=$('.gr-nav').outerHeight();
    var sub_h=$('#subheader').outerHeight();
    var c_h=30;
    
    var top=nav_h+c_h;
    
    if (subvisible) top+=sub_h;
    
    
    
    $('.gerror').css('top',top);
    
    
    $('.gerror').find('span').text(message);
    $('.gerror').fadeIn(500);
    
    errorT=window.setTimeout(function(){
        $('.gerror').fadeOut(500,function(){
            $('.gerror').remove();
        
        });
    }, 6000);

}

function ShowSucc($title, message){
    if ($('.gerror').length>0)
    {
        $('.gerror').remove();
    }
    if (errorT)
    {
        window.clearTimeout(errorT);
    }
    $('.has-error').removeClass('has-error');

    
       
    
    $('#content').append('<div class="alert alert-success alert-dismissable gerror"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>' + $title + '</strong> <span></span></div>');
    
    
    var subvisible=isScrolledIntoView($('#subheader'));
    
    var nav_h=$('.gr-nav').outerHeight();
    var sub_h=$('#subheader').outerHeight();
    var c_h=30;
    
    var top=nav_h+c_h;
    
    if (subvisible) top+=sub_h;
    
    
    
    $('.gerror').css('top',top);
    $('.gerror').find('span').text(message);
    $('.gerror').fadeIn(500);
    
    errorT=window.setTimeout(function(){
        $('.gerror').fadeOut(500,function(){
            $('.gerror').remove();
        
        });
    }, 6000);

}

function isScrolledIntoView(elem)
{
    if ($(elem).length==0) return false;
    
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();


    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();

    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}

function Verify(searchin)
{
    if (searchin==undefined) searchin=$('body');
    
    var haserror=false;
    var id='';
    var message='';
    
    $('.nec',searchin).each(function(e){
        
        if (!$(this).val())
        {
            id=$(this).attr('id');
           
            haserror=true;
            var p=$('#'+id).parents('.form-group').children('label').text();
            message='Please enter ' + p + '.';
            
            return false;
        }else if ($(this).hasClass('email')){
            if (!isEmail($(this).val()))
            {
                id=$(this).attr('id');
                haserror=true;
                message='Enter a valid email address.';
                return false;
            }
        }
    });
    
    if (haserror){
      
        ShowError(id, message );
        return false;
    }
    

    
    return true;











}



function stripeResponseHandler(status, response) {
    if (response.error) {
        //  console.log(response.error)
        var id='card1';
        
        switch (response.error.code) {
            case 'incorrect_number':
            case 'invalid_number':
            case 'expired_card':
            case 'card_declined':
            case 'missing':
            case 'processing_error':
                id='card1';
                break;
        
            case 'invalid_cvc':
            case 'incorrect_cvc':
                id='cardcvc';
                break;
        
            case 'incorrect_zip':
                id='bzip';
                break;
    
            case 'invalid_expiry_month':
                id='cardmonth';
                break;
        
            case 'invalid_expiry_year':
                id='cardyear';
                break;
        
        }
        
        ShowError(id,response.error.message);
        $('.signup-btn').button('reset');
    
    } else {
        
        var token = response['id'];
        $('#cardtoken').val(token);
        
        var post={};
        post['fields']=GetFields();
        if ($('#birthyear').val() && $('#birthmonth').val() && $('#birthday').val()){
            post['fields']['birthdate']=$('#birthyear').val() + '-' + $('#birthmonth').val() + '-' + $('#birthday').val();
        }
       
        
        
        
        ajaxify('signup', post, function(){
            
            ShowSucc('Thank you!','You are submitted to get your Instagram pictures printed monthly.');
            
            setTimeout(function(){
                location.href=insta_url;
            }, 5000);
            
            
        // $('.signup-btn').button('reset');
        }, function(){
            
            $('.signup-btn').button('reset');
        });
        
        
        
}
}

function LoadOldUser(){

    if ($('#olduser').text())
    {
           
        var fields = jQuery.parseJSON($('#olduser').text());
        
        if (fields['birthdate']){
            fields['birthyear']=fields['birthdate'].split('-')[0];
            fields['birthmonth']=fields['birthdate'].split('-')[1];
            fields['birthday']=fields['birthdate'].split('-')[2];
            
        }
       
       
        
        for (id in fields)
        {
            if (id=='likes' || id=='unlimit' || id=='first' || id=='bytag')
            {
            
                if (fields[id]==1){
                   
                    $('#' + id).prop('checked', true);
                }
                    
            }else if (id!='igtoken') {
                $('#' + id).val(fields[id]);
            }
            
        }
    }

}
function GetFields(searchin){
    
    if (searchin==undefined) searchin=$('body');
    
    
    var fields = {};
    $('.inpost',searchin).each(function(){
        if ($(this).attr('type')=='checkbox'){
            fields[$(this).attr('id')]= $(this).is(':checked')?1:0;
            
        }else if ($(this).attr('type')=='radio'){
            fields[$(this).attr('id')]= $(this).is(':checked')?1:0;
            
        }else{
            fields[$(this).attr('id')]= $(this).val();
        }
    
    
    });
    return fields;

}

function isEmail(emailAddress) {
    
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    
    return pattern.test(emailAddress);
}

function ActivateNumeric() {
    $(".numeric").keydown(function(event) {
        // Allow: backspace, delete, tab, escape, and enter
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || (event.keyCode == 110 && $(this).val().indexOf('.')<0) || (event.keyCode == 190 && $(this).val().indexOf('.')<0) || 
            // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) || 
            // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault(); 
            }   
        }
    });
}


function ajaxify($function, postdata, succfunction, failfunction, asch) { // By MscEliot
    postdata['ajax'] = $function;
    var a = true;
    
    if (asch != undefined) {
        a = asch;
    }
    
    $.ajax({
        type: 'POST',
        url: ajaxurl,
        dataType: 'json',
        data: postdata,
        async: a,
        success: function(data) {
            if (data.error) {
                if (typeof failfunction == 'function') {
                    failfunction.call(this, data.message);
                }
            } else {
                if (typeof succfunction == 'function') {
                    succfunction.call(this, data);
                }
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            if (typeof failfunction == 'function') {
                failfunction.call(this, 'Connection error');
            }
        }
    })
}


    
    
    

